/**
 * 
 */
package ru.ibrae.cache;

/**
 * @author pvo
 * 
 */
public interface IDisposer<K, V> {
	void dispose( K key, V value );

}
