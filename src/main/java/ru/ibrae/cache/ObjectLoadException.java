package ru.ibrae.cache;

public class ObjectLoadException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5847647461591832918L;

	public ObjectLoadException( final String message, final Throwable cause ) {
		super( message, cause );
	}

	public ObjectLoadException( final String message ) {
		super( message );
	}

	public ObjectLoadException( final Throwable cause ) {
		super( cause );
	}

}
