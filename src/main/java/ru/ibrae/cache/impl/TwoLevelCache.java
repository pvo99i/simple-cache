package ru.ibrae.cache.impl;

import java.io.File;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.IConverter;
import ru.ibrae.cache.IDisposer;

/**
 * Двухровневый кэш (LRUMemoryCache - DiskCache). Данные в дисковый кэш попадают при переполнении кэша 1 уровня.
 * 
 * @author pvo
 * 
 * @param <K>
 * @param <V>
 */
public class TwoLevelCache<K, V> extends BaseTwoLevelCache<K, V> {

	protected TwoLevelCache( final ICache<K, V> firstLevelCache, final ICache<K, V> secondLevelCache ) {
		super( firstLevelCache, secondLevelCache );
	}

	private static <K, V> DiskCache<K, V> createDiskCache( final File cacheDirectory, final IConverter<V> converter, final IKeyToFileMapper<K> mapper ) {
		return new DiskCache<K, V>( cacheDirectory, converter, mapper );
	}

	private static <K, V> ICache<K, V> createFirstLevelCache( final int capacity, final IDisposer<K, V> disposer,
			final DiskCache<K, V> secondLevelCache ) {
		return new LRUMemoryCache<K, V>( capacity, wrap( disposer, secondLevelCache ) );
	}

	private static <K, V> IDisposer<K, V> wrap( final IDisposer<K, V> disposer, final DiskCache<K, V> diskCache ) {
		return new IDisposer<K, V>() {
			@Override
			public void dispose( final K key, final V value ) {
				diskCache.put( key, value );
				if ( disposer != null ) {
					disposer.dispose( key, value );
				}

			}
		};
	}

	public static <K, V> TwoLevelCache<K, V> newInstance( final int capacity, final IDisposer<K, V> disposer, final File cacheDirectory,
			final IConverter<V> converter, final IKeyToFileMapper<K> mapper ) {
		final DiskCache<K, V> diskCache = createDiskCache( cacheDirectory, converter, mapper );
		final ICache<K, V> firstLevelCache = createFirstLevelCache( capacity, disposer, diskCache );
		return new TwoLevelCache<K, V>( firstLevelCache, diskCache );
	}

	public static <K, V> TwoLevelCache<K, V> newInstance( final int capacity, final File cacheDirectory, final IConverter<V> converter,
			final IKeyToFileMapper<K> mapper ) {
		return newInstance( capacity, null, cacheDirectory, converter, mapper );
	}
}
