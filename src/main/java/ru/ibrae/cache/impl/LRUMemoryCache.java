package ru.ibrae.cache.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.IDisposer;

public class LRUMemoryCache<K, V> extends AbstractCache<K, V> {
	private final Map<K, V> values;
	private int capacity;

	public LRUMemoryCache( final int capacity ) {
		this( capacity, (ICacheLoader<K, V>) null );
	}

	public LRUMemoryCache( final int capacity, final ICacheLoader<K, V> cacheLoader ) {
		this( capacity, cacheLoader, null );
	}

	@SuppressWarnings( "unchecked" )
	public LRUMemoryCache( final int capacity, final ICacheLoader<K, V> cacheLoader, final IDisposer<K, V> disposer ) {
		super( cacheLoader );
		values = Collections.synchronizedMap( new CustomLRUMap<K, V>( capacity, disposer ) );
		this.capacity = capacity;
	}

	public LRUMemoryCache( final int capacity, final IDisposer<K, V> disposer ) {
		this( capacity, null, disposer );
	}

	public void clear() {
		values.clear();
	}

	public int getCapacity() {
		return capacity;
	}

	public int count() {
		return values.size();
	}

	@Override
	protected V internalGet( final K key ) {
		return values.get( key );
	}

	public void put( final K key, final V object ) {
		values.put( key, object );
	}

    @Override
    public Set<K> getKeys() {
        return new HashSet<K>(values.keySet());
    }
}
