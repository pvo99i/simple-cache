package ru.ibrae.cache.impl;

import java.io.File;

public interface IKeyToFileMapper<K> {

    /**
     * Converts key to file.
     * @param directory
     * @param key
     * @return
     */
    File getFileFor( File directory, K key );

    /**
     * Converts filename to key.
     * @param directory
     * @param file
     * @return
     */
    K getKeyFor(File directory, File file);
}
