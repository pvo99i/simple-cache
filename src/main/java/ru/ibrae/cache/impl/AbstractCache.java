/**
 * 
 */
package ru.ibrae.cache.impl;

import java.util.concurrent.atomic.AtomicLong;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.ObjectLoadException;

/**
 * @author pvo
 * 
 */
public abstract class AbstractCache<K, V> implements ICache<K, V> {

	private final ICacheLoader<K, V> cacheLoader;
	private final AtomicLong hits = new AtomicLong();
	private final AtomicLong misses = new AtomicLong();

	public AbstractCache( final ICacheLoader<K, V> cacheLoader ) {
		this.cacheLoader = cacheLoader == null ? emptyCacheLoader() : cacheLoader;
	}

	private ICacheLoader<K, V> emptyCacheLoader() {
		return new ICacheLoader<K, V>() {

			public V load( final K key ) throws ObjectLoadException {
				return null;
			}
		};
	}

	public AbstractCache() {
		this( null );
	}

	protected abstract V internalGet( K key ) throws ObjectLoadException;

	public final V get( final K key ) throws ObjectLoadException {
		V result = internalGet( key );
		if ( result == null ) {
			misses.incrementAndGet();
			result = cacheLoader.load( key );
			if ( result != null ) {
				put( key, result );
			}
		} else {
			hits.incrementAndGet();
		}
		return result;
	}

	public boolean isEmpty() {
		return count() == 0;
	}

	// Statistics

	public long getHits() {
		return hits.get();
	}

	public long getMisses() {
		return misses.get();
	}

	public double getHitRatio() {
		return (double) getHits() / getMisses();
	}

	@Override
	public String toString() {
		return getClass().getName() + ": [count=" + count() + ", hits=" + getHits() + ", misses=" + getMisses() + ", ratio=" + getHitRatio() + "]";
	}
}
