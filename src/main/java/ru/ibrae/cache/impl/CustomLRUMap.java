package ru.ibrae.cache.impl;

import org.apache.commons.collections.map.LRUMap;

import ru.ibrae.cache.IDisposer;

public class CustomLRUMap<K, V> extends LRUMap {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7979273696395417518L;

	private transient  final IDisposer<K, V> disposer;

	public CustomLRUMap( final int capacity ) {
		this( capacity, null );
	}

	public CustomLRUMap( final int capacity, final IDisposer<K, V> disposer ) {
		super( capacity );
		this.disposer = disposer == null ? emptyDisposer() : disposer;
	}

	private IDisposer<K, V> emptyDisposer() {
		return new IDisposer<K, V>() {
			public void dispose( final K key, final V value ) {
			}
		};
	}

	@SuppressWarnings( "unchecked" )
	@Override
	protected boolean removeLRU( final LinkEntry entry ) {
		disposer.dispose( (K) entry.getKey(), (V) entry.getValue() );
		return true;
	}

	public IDisposer<K, V> getDisposer() {
		return disposer;
	}

}
