package ru.ibrae.cache.impl;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.ObjectLoadException;

import java.util.HashSet;
import java.util.Set;

/**
 * Двухуровневый кэш, в который данные попадают через загрузчик кэша второго уровеня.
 * 
 * @author pvo
 * 
 * @param <K>
 * @param <V>
 */
public class BaseTwoLevelCache<K, V> extends AbstractCache<K, V> {

	private ICache<K, V> firstLevelCache;
	private ICache<K, V> secondLevelCache;

	public BaseTwoLevelCache( final ICache<K, V> firstLevelCache, final ICache<K, V> secondLevelCache ) {
		super( new ICacheLoader<K, V>() {

			public V load( final K key ) throws ObjectLoadException {
				return secondLevelCache.get( key );
			}
		} );
        this.firstLevelCache = firstLevelCache;
        this.secondLevelCache = secondLevelCache;
	}

	public synchronized void put( final K key, final V object ) {
		firstLevelCache.put( key, object );
	}

	public synchronized void clear() {
		firstLevelCache.clear();
		secondLevelCache.clear();
	}

	public synchronized int getCapacity() {
		return firstLevelCache.getCapacity() + secondLevelCache.getCapacity();
	}

	public synchronized int count() {
		return firstLevelCache.count() + secondLevelCache.count();
	}

	@Override
	protected synchronized V internalGet( final K key ) throws ObjectLoadException {
		return firstLevelCache.get( key );
	}

	// For tests
	ICache<K, V> getFirstLevelCache() {
		return firstLevelCache;
	}

	ICache<K, V> getSecondLevelCache() {
		return secondLevelCache;
	}

    @Override
    public synchronized Set<K> getKeys() {
        Set<K> firstLevelKeys = getFirstLevelCache().getKeys();
        Set<K> secondLevelKeys = getSecondLevelCache().getKeys();
        HashSet<K> result = new HashSet<K>( firstLevelKeys.size() + secondLevelKeys.size() );
        result.addAll(firstLevelKeys);
        result.addAll(secondLevelKeys);
        return result;
    }

    /**
     * Shutdowns the cache. Pulls all contents from first level cache to the second level cache.
     */
    public synchronized void shutdown() throws ObjectLoadException {
        for (K key : getFirstLevelCache().getKeys()) {
            getSecondLevelCache().put(key, getFirstLevelCache().get(key));
        }
        getFirstLevelCache().clear();
    }
}
