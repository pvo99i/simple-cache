package ru.ibrae.cache.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.io.FileUtils;

import ru.ibrae.cache.FatalCacheException;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.IConverter;
import ru.ibrae.cache.ObjectLoadException;

/**
 * Дисковый кэш неограниченного размера.
 * 
 * @author pvo
 * 
 * @param <K>
 * @param <V>
 */
public class DiskCache<K, V> extends AbstractCache<K, V> {

	private static final int CAPACITY_UNLIMITED = 0; //Integer.MAX_VALUE;

	private static final String UNABLE_CREATE_DIRECTORY = "Unable to create cache directory '%s'";

	private final File directory;
	private final IKeyToFileMapper<K> keyToFileMapper;
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private final IConverter<V> converter;

	public DiskCache( final File directory, final ICacheLoader<K, V> cacheLoader, final IConverter<V> converter,
			final IKeyToFileMapper<K> fileTranslator ) {
		super( cacheLoader );
		this.directory = directory;
		this.keyToFileMapper = fileTranslator;
		this.converter = converter;
		if ( !( directory.exists() || directory.mkdirs() ) ) {
			throw new FatalCacheException( String.format( UNABLE_CREATE_DIRECTORY, directory.getAbsolutePath() ) );
		}
	}

	public DiskCache( final File cacheDirectory, final IConverter<V> converter, final IKeyToFileMapper<K> mapper ) {
		this( cacheDirectory, null, converter, mapper );
	}

	@Override
	public synchronized void put( final K key, final V object ) {
		final Callable<Void> operation = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				final OutputStream outputStream = getOutputStream( key );
				try {
					converter.write( object, outputStream );
				} finally {
					outputStream.close();
				}
				return null;
			}
		};

		performWrite( operation );
	}

	@Override
	public synchronized void clear() {
		final Callable<Void> operation = new Callable<Void>() {

			@Override
			public Void call() throws Exception {
				FileUtils.cleanDirectory( directory );
				return null;
			}
		};
		performWrite( operation );
	}

	@Override
	public int getCapacity() {
		return CAPACITY_UNLIMITED;
	}

	@Override
	public int count() {
		final Callable<Integer> operation = new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {
				return countFiles( directory );
			}
		};
		return performRead( operation );
	}

	public long getSizeInBytes() {
		final Callable<Long> operation = new Callable<Long>() {
			@Override
			public Long call() throws Exception {
				return totalSize( directory );
			}
		};
		return performRead( operation );
	}

	public File getDirectory() {
		return directory;
	}

	@Override
	protected V internalGet( final K key ) throws ObjectLoadException {
		final Callable<V> operation = new Callable<V>() {
			@Override
			public V call() throws Exception {
				V result = null;
				final File file = keyToFileMapper.getFileFor( getDirectory(), key );
				if ( file.exists() ) {
					final InputStream inputStream = new BufferedInputStream( new FileInputStream( file ) );
					try {
						result = converter.read( inputStream );
					} finally {
						inputStream.close();
					}
				}
				return result;
			}
		};
		return performRead( operation );
	}

	private int countFiles( final File directory ) {
		int result = 0;
		for ( final File file : directory.listFiles() ) {
			if ( file.isDirectory() ) {
				result += countFiles( file );
			} else {
				result++;
			}
		}
		return result;
	}

    private Set<File> collectFiles(File directory, Set<File> files) {
        for( File file : directory.listFiles() ) {
            if ( file.isFile() ) {
                files.add(file);
            } else {
                collectFiles(file, files);
            }
        }
        return files;
    }


	private long totalSize( final File directory ) {
		int result = 0;
		for ( final File file : directory.listFiles() ) {
			if ( file.isDirectory() ) {
				result += totalSize( file );
			} else {
				result += file.length();
			}
		}
		return result;
	}

	private OutputStream getOutputStream( final K key ) throws IOException {
		final File file = keyToFileMapper.getFileFor( getDirectory(), key );
		final File parent = file.getParentFile();
		if ( !parent.exists() ) {
			parent.mkdirs();
		}
		return new BufferedOutputStream( new FileOutputStream( file ) );
	}

	private <T> T performRead( final Callable<T> operation ) {
		return perform( lock.readLock(), operation );
	}

	private <T> T performWrite( final Callable<T> operation ) {
		return perform( lock.writeLock(), operation );
	}

	private static <T> T perform( final Lock l, final Callable<T> operation ) {
		l.lock();
		try {
			return operation.call();
		} catch ( final Exception ex ) {
			throw new FatalCacheException( ex );
		} finally {
			l.unlock();
		}
	}

    @Override
    public Set<K> getKeys() {
        Callable<Set<K>> operation = new Callable<Set<K>>() {
            @Override
            public Set<K> call() throws Exception {
                Set<File> files = collectFiles(getDirectory(), new HashSet<File>());
                HashSet<K> result = new HashSet<K>(files.size());
                for (File file : files) {
                    result.add(keyToFileMapper.getKeyFor(getDirectory(), file));
                }
                return result;
            }
        };
        return performRead(operation);
    }
}
