/**
 * 
 */
package ru.ibrae.cache;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author pvo
 * 
 */
public interface IConverter<T> {

	void write( T object, OutputStream outputStream ) throws IOException;

	T read( InputStream inputStream ) throws IOException;

}
