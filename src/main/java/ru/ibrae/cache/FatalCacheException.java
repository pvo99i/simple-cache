/**
 * 
 */
package ru.ibrae.cache;

/**
 * @author pvo
 * 
 */
public class FatalCacheException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4919887996368307281L;

	public FatalCacheException() {
		super();
	}

	public FatalCacheException( final String message, final Throwable cause ) {
		super( message, cause );
	}

	public FatalCacheException( final String message ) {
		super( message );
	}

	public FatalCacheException( final Throwable cause ) {
		super( cause );
	}

}
