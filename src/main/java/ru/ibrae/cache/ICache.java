/**
 * 
 */
package ru.ibrae.cache;

import java.util.Set;

/**
 * @author pvo
 * 
 */
public interface ICache<K, V> {

	V get( K key ) throws ObjectLoadException;

	void put( K key, V object );

	void clear();

	int getCapacity();

	int count();

	boolean isEmpty();

    Set<K> getKeys();

}
