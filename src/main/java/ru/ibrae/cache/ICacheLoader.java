/**
 * 
 */
package ru.ibrae.cache;

/**
 * @author pvo
 * 
 */
public interface ICacheLoader<K, V> {
	V load( K key ) throws ObjectLoadException;

}
