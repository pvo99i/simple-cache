/**
 * 
 */
package ru.ibrae.cache.impl;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;

/**
 * @author pvo
 * 
 */
public class BaseTwoLevelCacheTest extends DiskCacheTest {

	@Override
	protected ICache<String, Integer> createCache( final ICacheLoader<String, Integer> loader ) {
		return new BaseTwoLevelCache<String, Integer>( new LRUMemoryCache<String, Integer>( CACHE_CAPACITY ), super.createCache( loader ) );
	}

	@Override
	protected void afterMultithreadedTest( final ICache<String, Integer> cache ) {

	}

}
