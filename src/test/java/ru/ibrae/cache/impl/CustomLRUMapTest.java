package ru.ibrae.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import ru.ibrae.cache.IDisposer;

public class CustomLRUMapTest {

	private final int cacheSize = 10;
	private final int iterations = 100;
	private int disposedCouter = 0;

	private void incDisposed( final String object ) {
		assertEquals( String.valueOf( disposedCouter ), object );
		disposedCouter++;
	}

	@Test
	public void testDisposing() {
		final IDisposer<String, String> disposer = new IDisposer<String, String>() {
			public void dispose( final String key, final String value ) {
				incDisposed( value );
			}

		};
		disposedCouter = 0;

		final CustomLRUMap<String, String> map = new CustomLRUMap<String, String>( cacheSize, disposer );
		for ( int i = 0; i < iterations; i++ ) {
			final String value = String.valueOf( i );
			map.put( value, value );
		}

		assertEquals( cacheSize, map.size() );
		assertEquals( iterations - cacheSize, disposedCouter );
	}

	@Test
	public void testWithoutDisposer() {
		final CustomLRUMap<String, String> map = new CustomLRUMap<String, String>( 10 );
		assertNotNull( map.getDisposer() );
	}

}
