package ru.ibrae.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.SerializationUtils;
import org.junit.After;

import ru.ibrae.cache.FatalCacheException;
import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.IConverter;

public class DiskCacheTest extends AbstractCacheTest {

    static final File directory = new File( ".test" );
	private static final long INTEGER_SIZE_IN_BYTES;
	static {
		final Integer i = Integer.valueOf( 1 );
		INTEGER_SIZE_IN_BYTES = SerializationUtils.serialize( i ).length;
	}

    private static final String A_B_C_D = "a/b/c/d/";

    protected final IConverter<Integer> converter = new IConverter<Integer>() {

		@Override
		public void write( final Integer object, final OutputStream outputStream ) throws IOException {
			final ObjectOutputStream objectOutputStream = new ObjectOutputStream( outputStream );
			objectOutputStream.writeObject( object );
		}

		@Override
		public Integer read( final InputStream inputStream ) throws IOException {
			final ObjectInputStream objectInputStream = new ObjectInputStream( inputStream );
			try {
				return (Integer) objectInputStream.readObject();
			} catch ( final ClassNotFoundException e ) {
				throw new FatalCacheException( e );
			}
		}
	};

	protected final IKeyToFileMapper<String> mapper = new IKeyToFileMapper<String>() {

		@Override
		public File getFileFor( final File directory, final String key ) {
			return new File( directory, A_B_C_D + key );
		}

        @Override
        public String getKeyFor(File directory, File file) {
            int i = file.getAbsolutePath().lastIndexOf('/');
            return file.getAbsolutePath().substring(i+1);
        }
    };

	@After
	public/*static*/void cleanup() throws IOException {
		FileUtils.deleteDirectory( directory );
	}

	@Override
	protected ICache<String, Integer> createCache( final ICacheLoader<String, Integer> loader ) {
		return new DiskCache<String, Integer>( directory, loader, converter, mapper );
	}

	@Override
	protected void afterMultithreadedTest( final ICache<String, Integer> cache ) {
		final DiskCache<String, Integer> diskCache = (DiskCache<String, Integer>) cache;
		final int count = diskCache.count();
		assertTrue( count > 0 );
		assertEquals( count * INTEGER_SIZE_IN_BYTES, diskCache.getSizeInBytes() ); // TODO capacity в дисковом кэше убрать.
	}
}
