package ru.ibrae.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.ObjectLoadException;

public class LRUMemoryCacheTest extends AbstractCacheTest {

	@Override
	protected ICache<String, Integer> createCache( final ICacheLoader<String, Integer> loader ) {
		return new LRUMemoryCache<String, Integer>( CACHE_CAPACITY, loader );
	}

	@Override
	protected void afterMultithreadedTest( final ICache<String, Integer> cache ) {
		assertTrue( cache.count() <= CACHE_CAPACITY );
	}

	@Test
	public void testCacheWithoutLoader() throws ObjectLoadException {
		final ICache<String, Integer> cache = createCacheWithoutLoader();

		for ( int i = 0; i < 100; i++ ) {
			cache.put( String.valueOf( i ), Integer.valueOf( i ) );
			cache.get( String.valueOf( CACHE_KEY ) );
		}

		assertNull( cache.get( "qwe" ) );
		assertNull( cache.get( String.valueOf( 0 ) ) );

		assertEquals( CACHE_VALUE, cache.get( CACHE_KEY ) );

		cache.clear();
		assertTrue( cache.isEmpty() );
	}

}
