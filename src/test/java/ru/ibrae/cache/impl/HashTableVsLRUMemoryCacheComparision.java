package ru.ibrae.cache.impl;

import java.util.HashMap;
import java.util.Random;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ObjectLoadException;

public class HashTableVsLRUMemoryCacheComparision {

	private static final int COUNT = 1000000;

	private static final int REQUESTS = COUNT * 30;

	private static Random random = new Random( System.currentTimeMillis() );

	static interface IStorage {
		void put( String key, Integer value );

		Integer get( String key );

		String getName();
	}

	static class HashMapStorage implements IStorage {
		private final HashMap<String, Integer> source;

		public HashMapStorage( final HashMap<String, Integer> source ) {
			this.source = source;
		}

		public Integer get( final String key ) {
			return source.get( key );
		}

		public void put( final String key, final Integer value ) {
			source.put( key, value );
		}

		public String getName() {
			return source.getClass().getName();
		}
	}

	static class CacheStorage implements IStorage {
		private final ICache<String, Integer> source;

		public CacheStorage( final ICache<String, Integer> source ) {
			super();
			this.source = source;
		}

		public void put( final String key, final Integer value ) {
			source.put( key, value );
		}

		public Integer get( final String key ) {
			try {
				return source.get( key );
			} catch ( final ObjectLoadException e ) {
				e.printStackTrace();
				throw new RuntimeException( e );
			}
		}

		public String getName() {
			return source.getClass().getName();
		}

	}

	// @Test
	public void test() {
		for ( int i = 0; i < COUNT; i++ ) {
			Integer.valueOf( i );
		}
		final long hashMapTime = performWith( new HashMapStorage( new HashMap<String, Integer>( COUNT ) ) );
		final long cacheTime = performWith( new CacheStorage( new LRUMemoryCache<String, Integer>( COUNT ) ) );

		System.out.println( (double) hashMapTime / cacheTime );
	}

	private long performWith( final IStorage storage ) {
		final long start = System.currentTimeMillis();

		for ( int i = 0; i < COUNT; i++ ) {
			storage.put( String.valueOf( i ), Integer.valueOf( i ) );
		}

		for ( int i = 0; i < REQUESTS; i++ ) {
			storage.get( String.valueOf( random.nextInt( COUNT ) ) );
		}

		final long elapsed = System.currentTimeMillis() - start;
		System.out.println( String.format( "With %s: Elapsed: %d msec.", storage.getName(), elapsed ) );
		return elapsed;
	}
}
