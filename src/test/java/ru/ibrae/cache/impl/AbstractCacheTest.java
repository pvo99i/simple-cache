package ru.ibrae.cache.impl;

import java.util.HashSet;
import java.util.Random;

import org.junit.Test;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.ObjectLoadException;

import static org.junit.Assert.*;

public abstract class AbstractCacheTest {

	protected static final int CACHE_CAPACITY = 10;

	protected static int val = 1;
	protected static final String CACHE_KEY = String.valueOf( val );
	protected static final Integer CACHE_VALUE = Integer.valueOf( val );

	protected static final int NUMBER_OF_A_BEAST = 666;

	protected static final String KEY_FOR_EXCEPTION = String.valueOf( NUMBER_OF_A_BEAST );
	protected static final int MAX_KEY = NUMBER_OF_A_BEAST;

	@Test
	public void testEmptiness() {
		assertTrue( createCacheWithoutLoader().isEmpty() );
	}

	@Test
	public void testCacheWithLoader() throws ObjectLoadException {
		final ICache<String, Integer> cache = createCacheWithLoader();

		try {
			cache.get( KEY_FOR_EXCEPTION );
			fail();
		} catch ( final ObjectLoadException e ) {
			// Expected exception
		}

		assertNull( cache.get( String.valueOf( MAX_KEY + 1 ) ) );

		for ( int i = 0; i < 100; i++ ) {
			assertNotNull( cache.get( String.valueOf( i ) ) );
		}
	}

	protected ICache<String, Integer> createCacheWithLoader() {
		final ICacheLoader<String, Integer> loader = new ICacheLoader<String, Integer>() {

			public Integer load( final String key ) throws ObjectLoadException {
				if ( key.equals( KEY_FOR_EXCEPTION ) ) {
					throw new ObjectLoadException( "Test exception" );
				}
				final int i = Integer.parseInt( key );
				if ( i < MAX_KEY ) {
					return new Integer( i );
				}
				return null;
			}

		};

		return createCache( loader );
	}

	protected abstract ICache<String, Integer> createCache( final ICacheLoader<String, Integer> loader );

	protected ICache<String, Integer> createCacheWithoutLoader() {
		return createCache( null );
	}

    protected int getTestKeysCount(int cacheCapacity) {
        return cacheCapacity > 1000? 1000: cacheCapacity;
    }

    @Test
    public void testGetKeys() {
        ICache<String,Integer> cache = createCacheWithoutLoader();
        try {
            HashSet<String> expectedKeys = new HashSet<String>();
            int keysCount = getTestKeysCount(cache.getCapacity());
            for(int i = 0; i < keysCount; i++) {
                String key = String.valueOf(i);
                cache.put(key, Integer.valueOf(i));
                expectedKeys.add(key);
            }

            assertEquals(expectedKeys.size(), cache.getKeys().size());
            HashSet<String> realKeys = new HashSet<String>( cache.getKeys() );
            for (String expectedKey : expectedKeys) {
                assertTrue(realKeys.contains(expectedKey));
            }
        } finally {
            cache.clear();
        }
    }

	@Test
	public void simpleMultiThreadedTest() throws InterruptedException {

		final ICache<String, Integer> cache = createCacheWithLoader();
		final int ITERATIONS = 300;

		final Runnable reader = createReader( cache, ITERATIONS );
		final Runnable writer = createWriter( cache, ITERATIONS );

		final int READERS_COUNT = 50;
		final int WRITERS_COUNT = 30;
		final int THREADS_COUNT = READERS_COUNT + WRITERS_COUNT;

		final Thread[] threads = new Thread[ THREADS_COUNT ];

		for ( int i = 0; i < READERS_COUNT; i++ ) {
			threads[ i ] = new Thread( reader );
			threads[ i ].start();
		}

		for ( int i = READERS_COUNT; i < THREADS_COUNT; i++ ) {
			threads[ i ] = new Thread( writer );
			threads[ i ].start();
		}

		for ( int i = 0; i < THREADS_COUNT; i++ ) {
			threads[ i ].join();
		}

		System.out.println( cache );
		afterMultithreadedTest( cache );

	}

	protected abstract void afterMultithreadedTest( final ICache<String, Integer> cache );

	private Runnable createWriter( final ICache<String, Integer> cache, final int ITERATIONS ) {
		return new Runnable() {
			public void run() {
				final Random random = new Random( System.currentTimeMillis() );

				for ( int i = 0; i < ITERATIONS; i++ ) {
					final int nextInt = random.nextInt( NUMBER_OF_A_BEAST );
					cache.put( String.valueOf( nextInt ), Integer.valueOf( nextInt ) );
					Thread.yield();
					try {
						Thread.sleep( random.nextInt( 10 ) );
					} catch ( final InterruptedException e ) {
					}
				}

			}
		};
	}

	private Runnable createReader( final ICache<String, Integer> cache, final int ITERATIONS ) {
		return new Runnable() {

			public void run() {
				final Random random = new Random( System.currentTimeMillis() );

				for ( int i = 0; i < ITERATIONS; i++ ) {
					try {
						cache.get( String.valueOf( random.nextInt( NUMBER_OF_A_BEAST ) ) );
						Thread.yield();
					} catch ( final ObjectLoadException e ) {
						e.printStackTrace();
					}
				}
			}
		};
	}

}
