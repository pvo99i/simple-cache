package ru.ibrae.cache.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import ru.ibrae.cache.ICache;
import ru.ibrae.cache.ICacheLoader;
import ru.ibrae.cache.ObjectLoadException;

public class TwoLevelCacheTest extends DiskCacheTest {

	@Override
	protected ICache<String, Integer> createCache( final ICacheLoader<String, Integer> loader ) {
		return TwoLevelCache.newInstance( CACHE_CAPACITY, directory, converter, mapper );
	}

	@Test
	public void testObjectPulling() throws ObjectLoadException {
		final TwoLevelCache<String, Integer> cache = (TwoLevelCache<String, Integer>) createCache( null );
		try {
			for ( int i = 0; i < CACHE_CAPACITY; i++ ) {
				cache.put( String.valueOf( i ), Integer.valueOf( i ) );
			}
			cache.put( "100500", Integer.valueOf( 100500 ) );
			assertEquals( Integer.valueOf( 0 ), cache.getSecondLevelCache().get( "0" ) );
		} finally {
			cache.clear();
		}

	}

	@Override
	@Test
	public void testCacheWithLoader() throws ObjectLoadException {
		// В TwoLevelCache нет лоадера.
	}

	@Override
	protected void afterMultithreadedTest( final ICache<String, Integer> cache ) {
		// TODO Auto-generated method stub

	}

    @Override
    protected int getTestKeysCount(int cacheCapacity) {
        return CACHE_CAPACITY + 1;
    }

    @Test
    public void voidTestShutdown() throws ObjectLoadException {
        String key = "key";
        Integer value = Integer.valueOf(10);

        TwoLevelCache<String, Integer> cache = (TwoLevelCache<String, Integer>) createCache(null);
        try {
            cache.put(key, value);
            cache.shutdown();
            assertTrue(cache.getFirstLevelCache().isEmpty());
            assertTrue(cache.getSecondLevelCache().getKeys().contains(key));

            cache = (TwoLevelCache<String, Integer>) createCache(null);
            assertEquals(value, cache.get(key));
        }   finally {
            cache.clear();
        }
    }
}
